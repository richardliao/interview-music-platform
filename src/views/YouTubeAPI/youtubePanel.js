import React, { Component } from "react";
class YoutubePanel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      whichPlay: ""
    };
  }
  componentWillReceiveProps(nextProps) {
    //  console.log(nextProps)
    if (nextProps.whichPlay != this.props.whichPlay) {
      // console.log(`onFunction: ${nextProps.whichPlaybtn}`);
    }
  }
  render() {
    const { nowPlay, isFavorite } = this.props;
    return (
      <div className="Footer-Block">
        <div className="d-flex align-items-center">
          <div className="flex-shrink-0">
            <button
              className="btn btn-link"
              onClick={e => this.props.onPrev(e)}
            >
              <i class="fas fa-backward" />
            </button>
            {this.props.whichPlay === "playall" && (
              <button
                className="btn btn-play"
                onClick={e => this.props.onPlayVideoAll(e)}
              >
                <i className="fas fa-play" />
              </button>
            )}
            {this.props.whichPlay === "pause" && (
              <button
                className="btn btn-play"
                onClick={e => this.props.onPause(e)}
              >
                <i className="fas fa-pause" />
              </button>
            )}
            {this.props.whichPlay === "play" && (
              <button
                className="btn btn-play"
                onClick={e => this.props.onPlay(e)}
              >
                <i className="fas fa-play" />
              </button>
            )}

            <button
              className="btn btn-link"
              onClick={e => this.props.onNext(e)}
            >
              <i class="fas fa-forward" />
            </button>
          </div>
          <div>
            {isFavorite === "kkboxList" && nowPlay && nowPlay.length !== 0 && (
              <span>{nowPlay && nowPlay[0].name}</span>
            )}
            {isFavorite === "followme" && nowPlay && nowPlay.length !== 0 && (
              <span>{nowPlay && nowPlay[0].snippet.title}</span>
            )}
            {isFavorite === "favorite" && nowPlay && nowPlay.length !== 0 && (
              <span>{nowPlay && nowPlay[0].snippet.title}</span>
            )}
          </div>
        </div>
      </div>
    );
  }
}
export default YoutubePanel;
