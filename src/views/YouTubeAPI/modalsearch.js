import React from "react";
import Modal from "../../components/Modal";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import { parseDate } from "../../utils/config";
const ModalSearch = ({
  datas,
  keyword,
  handleHide2,
  handleChange,
  onPlaySearchAllList,
  onSearch,
  optTypeCheck,
  optTimeCheck,
  optPopularCheck,
  onFavorite,
  onplayVideo
}) => {
  const renderGrid = e => {
    return (
      <nav className="play-slider">
        {datas.items.map((item, idx) => {
          return (
            <div className="slider-card mb-2">
              <button
                className="btn btn-link btn-favorite"
                onClick={e => onFavorite(e, item)}
              >
                <img
                  draggable="false"
                  className="emoji"
                  alt="💓"
                  src="https://twemoji.maxcdn.com/2/72x72/1f493.png"
                  style={{ width: "26px" }}
                />
              </button>
              <a
                onClick={e => onplayVideo(e, item)}
                href="#"
                className="list-group-item list-group-item-action flex-column align-items-start p-0 good-shadow"
                target="_blank"
              >
                <div className="row flex-nowrap">
                  <div className="list-left d-flex align-items-start">
                    <img
                      className="card-img-top"
                      src={item.snippet.thumbnails.medium.url}
                      style={{ width: "120px" }}
                    />
                  </div>
                  <div className="list-right d-flex justify-content-between w-100 p-2">
                    <div className="list-textContent ml-2">
                      <h6> {item.snippet.title}</h6>
                      <p className="mb-0" style={{ "font-size": "12px" }}>
                        {parseDate(item.snippet.publishedAt)}
                        <span className="ml-2">
                          觀看人數: {item.statistics.viewCount}
                        </span>
                      </p>
                    </div>
                  </div>
                </div>
              </a>
            </div>
          );
        })}
      </nav>
    );
  };
  return (
    <Modal>
      <div className="modal-wrap">
        <div className="container" style={{ maxWidth: "800px" }}>
          <div className="searchBox">
            <div className="input-group d-flex flex-nowrap">
              <button
                className="btn btn-link close-modal"
                onClick={handleHide2}
              >
                <i className="fas fa-chevron-left" />
              </button>
              <input
                className="form-control w-100"
                name="keyword"
                value={keyword}
                onChange={handleChange}
                placeholder="請輸入關鍵字"
              />
              <button
                onClick={onSearch}
                className="btn btn-primary white-space-pre"
              >
                <img
                  src={require("../../static/img/search.svg")}
                  style={{ width: "26px", filter: "brightness(0) invert(1)" }}
                />
              </button>
            </div>
            <div className="searchBox-filter">
              <div className="form-group d-flex align-items-center mb-0">
                <label className="mr-3">類型</label>
                <RadioGroup
                  aria-label="Gender"
                  name="optTypeCheck"
                  value={optTypeCheck}
                  onChange={handleChange}
                  className="d-flex flex-row"
                >
                  {[
                    { value: "video", label: "影片" },
                    { value: "live", label: "直撥" }
                  ].map((v, i) => {
                    return (
                      <FormControlLabel
                        key={i}
                        value={v.value}
                        control={<Radio />}
                        label={v.label}
                      />
                    );
                  })}
                </RadioGroup>
              </div>

              {optTypeCheck === "live" ? null : (
                <React.Fragment>
                  <div className="form-group d-flex align-items-center mb-0">
                    <label className="mr-3">日期</label>
                    <RadioGroup
                      aria-label="Gender"
                      name="optTimeCheck"
                      value={optTimeCheck}
                      onChange={handleChange}
                      className="d-flex flex-row"
                    >
                      {[
                        { value: "all", label: "全部" },
                        { value: "month", label: "一個月內" }
                      ].map((v, i) => {
                        return (
                          <FormControlLabel
                            key={i}
                            value={v.value}
                            control={<Radio />}
                            label={v.label}
                          />
                        );
                      })}
                    </RadioGroup>
                  </div>
                  <div className="form-group d-flex align-items-center mb-0">
                    <label className="mr-3">排序</label>
                    <RadioGroup
                      aria-label="Gender"
                      name="optPopularCheck"
                      value={optPopularCheck}
                      onChange={handleChange}
                      className="d-flex flex-row"
                    >
                      {[
                        { value: "popular", label: "熱門排序" },
                        { value: "date", label: "日期排序" }
                      ].map((v, i) => {
                        return (
                          <FormControlLabel
                            key={i}
                            value={v.value}
                            control={<Radio />}
                            label={v.label}
                          />
                        );
                      })}
                    </RadioGroup>
                  </div>
                </React.Fragment>
              )}
            </div>
            <div className="searchBox-viewBox">
              <div className="d-flex flex-nowrap mb-3 align-items-center">
                <h5 className="mb-0 mr-2">搜尋結果</h5>
                <button
                  onClick={onPlaySearchAllList}
                  className="btn btn-sm btn-primary"
                >
                  全部播放
                </button>
              </div>
              <div>{datas.items.length === 0 ? "查無資料" : renderGrid()}</div>
            </div>
          </div>
        </div>
      </div>
    </Modal>
  );
};

export default ModalSearch;
