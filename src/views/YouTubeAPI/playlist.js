import React from "react";

const PlayList = ({
  favoriteData,
  kkboxChartList,
  searchList,
  isFavorite,
  nowPlay,
  onplayVideo,
  onPlaykkboxToYou,
  kkTransYouTube,
  onFavorite,
  removeFavorite,
  addToFavorite
}) => {
  return (
    <ul>
      {isFavorite === "favorite"
        ? favoriteData &&
          favoriteData.map(val => {
            return (
              <li
                className={`good-shadow ${nowPlay === val.id &&
                  "good-gradient-primary"}`}
                onClick={e => onplayVideo(e, val)}
              >
                {nowPlay === val.id && (
                  <img
                    draggable="false"
                    className="emoji mr-3"
                    alt="🎧"
                    src="https://twemoji.maxcdn.com/2/72x72/1f3a7.png"
                    style={{ width: "20px" }}
                  />
                )}
                <div
                  className={`p-2 flex-shrink-0 photo ${nowPlay === val.id &&
                    "playing"}`}
                  style={{
                    backgroundImage: `url(${val.snippet.thumbnails.high.url})`,
                    width: "60px",
                    height: "60px",
                    backgroundSize: "cover",
                    borderRadius: "50%"
                  }}
                />
                <div className="mr-auto ml-2">
                  <span className="singer">{val.snippet.title}</span>
                </div>
                <div className="ml-auto">
                  <div className="btn-group">
                    <button
                      type="button"
                      className="btn btn-link"
                      data-toggle="dropdown"
                      aria-haspopup="true"
                      aria-expanded="false"
                    >
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="22"
                        height="22"
                        viewBox="0 0 24 24"
                        fill="none"
                        stroke="#333"
                        stroke-width="3"
                        stroke-linecap="square"
                        stroke-linejoin="arcs"
                      >
                        <circle cx="12" cy="12" r="1" />
                        <circle cx="12" cy="5" r="1" />
                        <circle cx="12" cy="19" r="1" />
                      </svg>
                    </button>
                    <div className="dropdown-menu">
                      <div className="dropdown-divider" />
                      <a
                        className="dropdown-item"
                        href="javascript:;"
                        onClick={e => removeFavorite(e, val)}
                      >
                        移除最愛
                      </a>
                    </div>
                  </div>
                </div>
              </li>
            );
          })
        : isFavorite === "followme"
        ? favoriteData &&
          favoriteData.map(val => {
            return (
              <li
                className={`good-shadow ${nowPlay === val.id &&
                  "good-gradient-primary"}`}
                onClick={e => onplayVideo(e, val)}
              >
                {nowPlay === val.id && (
                  <img
                    draggable="false"
                    className="emoji mr-3"
                    alt="🎧"
                    src="https://twemoji.maxcdn.com/2/72x72/1f3a7.png"
                    style={{ width: "20px" }}
                  />
                )}

                <div
                  className={`p-2 flex-shrink-0 photo ${nowPlay === val.id &&
                    "playing"}`}
                  style={{
                    backgroundImage: `url(${val.snippet.thumbnails.high.url})`,
                    width: "60px",
                    height: "60px",
                    backgroundSize: "cover",
                    borderRadius: "50%"
                  }}
                />
                <div className="mr-auto ml-2">
                  <span className="singer">{val.snippet.title}</span>
                </div>
                <div className="ml-auto">
                  <div className="btn-group">
                    <button
                      type="button"
                      className="btn btn-link"
                      data-toggle="dropdown"
                      aria-haspopup="true"
                      aria-expanded="false"
                    >
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="22"
                        height="22"
                        viewBox="0 0 24 24"
                        fill="none"
                        stroke="#333"
                        stroke-width="3"
                        stroke-linecap="square"
                        stroke-linejoin="arcs"
                      >
                        <circle cx="12" cy="12" r="1" />
                        <circle cx="12" cy="5" r="1" />
                        <circle cx="12" cy="19" r="1" />
                      </svg>
                    </button>
                    <div className="dropdown-menu">
                      <div className="dropdown-divider" />
                      <button
                        className="btn btn-link btn-favorite"
                        onClick={e => addToFavorite(e, val)}
                      >
                        <img
                          draggable="false"
                          className="emoji"
                          alt="💓"
                          src="https://twemoji.maxcdn.com/2/72x72/1f493.png"
                          style={{ width: "26px" }}
                        />
                        加到我的最愛
                      </button>
                    </div>
                  </div>
                </div>
              </li>
            );
          })
        : isFavorite === "kkboxList"
        ? kkboxChartList &&
          kkboxChartList.map(val => {
            return (
              <li
                className={`good-shadow ${nowPlay === val.id &&
                  "good-gradient-primary"}`}
                onClick={e => onPlaykkboxToYou(e, val)}
              >
                {nowPlay === val.id && (
                  <img
                    draggable="false"
                    className="emoji mr-3"
                    alt="🎧"
                    src="https://twemoji.maxcdn.com/2/72x72/1f3a7.png"
                    style={{ width: "20px" }}
                  />
                )}
                <div
                  className={`p-2 flex-shrink-0 photo ${nowPlay === val.id &&
                    "playing"}`}
                  style={{
                    backgroundImage: `url(${val.album.images[0].url})`,
                    width: "60px",
                    height: "60px",
                    backgroundSize: "cover",
                    borderRadius: "50%"
                  }}
                />
                <div className="mr-auto ml-2">
                  <span className="singer">{val.album.artist.name}</span>
                  <br />
                  <span className="songName">{val.name}</span>
                </div>
                <div className="ml-auto">
                  <div className="btn-group">
                    <button
                      type="button"
                      className="btn btn-link"
                      data-toggle="dropdown"
                      aria-haspopup="true"
                      aria-expanded="false"
                      onClick={e => {
                        e.preventDefault();
                        if (e.target.nodeName !== "BUTTON");
                      }}
                    >
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="22"
                        height="22"
                        viewBox="0 0 24 24"
                        fill="none"
                        stroke="#333"
                        stroke-width="3"
                        stroke-linecap="square"
                        stroke-linejoin="arcs"
                      >
                        <circle cx="12" cy="12" r="1" />
                        <circle cx="12" cy="5" r="1" />
                        <circle cx="12" cy="19" r="1" />
                      </svg>
                    </button>
                    <div className="dropdown-menu">
                      <div className="dropdown-divider" />
                      <button
                        className="btn btn-link btn-favorite"
                        onClick={async e => {
                          e.persist();
                          await kkTransYouTube(val, res => {
                            onFavorite(e, res);
                          });
                        }}
                      >
                        <img
                          draggable="false"
                          className="emoji"
                          alt="💓"
                          src="https://twemoji.maxcdn.com/2/72x72/1f493.png"
                          style={{ width: "26px" }}
                        />
                        加到我的最愛
                      </button>
                    </div>
                  </div>
                </div>
              </li>
            );
          })
        : isFavorite === "searchList"
        ? searchList &&
          searchList.map(val => {
            return (
              <li
                className={`good-shadow ${nowPlay === val.id &&
                  "good-gradient-primary"}`}
                onClick={e => onplayVideo(e, val)}
              >
                {nowPlay === val.id && (
                  <img
                    draggable="false"
                    className="emoji mr-3"
                    alt="🎧"
                    src="https://twemoji.maxcdn.com/2/72x72/1f3a7.png"
                    style={{ width: "20px" }}
                  />
                )}
                <div
                  className={`p-2 flex-shrink-0 photo ${nowPlay === val.id &&
                    "playing"}`}
                  style={{
                    backgroundImage: `url(${val.snippet.thumbnails.high.url})`,
                    width: "60px",
                    height: "60px",
                    backgroundSize: "cover",
                    borderRadius: "50%"
                  }}
                />
                <div className="mr-auto ml-2">
                  <span className="singer">{val.snippet.title}</span>
                </div>
                <div className="ml-auto">
                  <div className="btn-group">
                    <button
                      type="button"
                      className="btn btn-link"
                      data-toggle="dropdown"
                      aria-haspopup="true"
                      aria-expanded="false"
                    >
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="22"
                        height="22"
                        viewBox="0 0 24 24"
                        fill="none"
                        stroke="#333"
                        stroke-width="3"
                        stroke-linecap="square"
                        stroke-linejoin="arcs"
                      >
                        <circle cx="12" cy="12" r="1" />
                        <circle cx="12" cy="5" r="1" />
                        <circle cx="12" cy="19" r="1" />
                      </svg>
                    </button>
                    <div className="dropdown-menu">
                      <div className="dropdown-divider" />
                      <button
                        className="btn btn-link btn-favorite"
                        onClick={e => addToFavorite(e, val)}
                      >
                        <img
                          draggable="false"
                          className="emoji"
                          alt="💓"
                          src="https://twemoji.maxcdn.com/2/72x72/1f493.png"
                          style={{ width: "26px" }}
                        />
                        加到我的最愛
                      </button>
                    </div>
                  </div>
                </div>
              </li>
            );
          })
        : null}
    </ul>
  );
};

export default PlayList;
