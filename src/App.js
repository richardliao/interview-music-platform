import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.bundle.min";
import "./styles/main.scss";
import "./styles/good-ui.css";
import Youtube from "./views/YouTubeAPI/index";
import {Route} from 'react-router-dom'
function App() {
  return (
    <>
    <Route pathname="/" component={Youtube}/>
    </>
  );
}

export default App;
