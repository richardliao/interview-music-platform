import React, { Component } from "react";

export default class footer extends Component {
  render() {
    return (
      <div className="footer-wrap">
        <div className="container">
          <div className="footer-info">
            <span>本站網址：</span>
            <a href="https://richardliao.gitlab.io/lets-api" target="_blank">
              https://richardliao.gitlab.io/lets-api
            </a>
          </div>
        </div>
      </div>
    );
  }
}
